# PackageSearchResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**package** | **str** | The Debian source package for which up to 2 search results have been aggregated in &#x60;results&#x60;. | 
**results** | [**list[SearchResult]**](SearchResult.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# SearchResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**package** | **str** | The Debian source package containing this search result, including the full Debian version number. | 
**path** | **str** | Path to the file containing the this search result, starting with &#x60;package&#x60;. | 
**line** | **int** | Line number containing the search result. | 
**context_before** | **list[str]** | Up to 2 full lines before the search result (see &#x60;context&#x60;). | [optional] 
**context** | **str** | The full line containing the search result. | 
**context_after** | **list[str]** | Up to 2 full lines after the search result (see &#x60;context&#x60;). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


